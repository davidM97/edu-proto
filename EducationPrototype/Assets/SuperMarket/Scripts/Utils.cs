using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Pool
{
    public string tag;
    public GameObject prefab;
    public int size;
}

public class Utils : MonoBehaviour
{
    public static Utils instance;
    private void Awake()
    {
        if (!instance)
            instance = this;
        else
            Destroy(this);
    }
    public Dictionary<string, Queue<GameObject>> PoolObject(List<Pool> pools, Transform parent)
    {
        Dictionary<string, Queue<GameObject>> poolDictionary = new Dictionary<string, Queue<GameObject>>();
        foreach (Pool pool in pools)
        {
            Queue<GameObject> objectPool = new Queue<GameObject>();
            for (int i = 0; i < pool.size; i++)
            {
                GameObject obj = Instantiate(pool.prefab, parent);
                obj.SetActive(false);
                objectPool.Enqueue(obj);
            }
            poolDictionary.Add(pool.tag,objectPool);
        }
        return poolDictionary;
    }
    public GameObject SpawnFromPool(Dictionary<string,Queue<GameObject>> poolDictionary,string tag, Vector3 position,Quaternion rotation) 
    {
        if (!poolDictionary.ContainsKey(tag))
            return null;

        GameObject objectToSpawn = poolDictionary[tag].Dequeue();
        objectToSpawn.SetActive(true);
        poolDictionary[tag].Enqueue(objectToSpawn);
        return objectToSpawn;
    }

}
