using TMPro;
using UnityEngine;
using UnityEngine.UI;
public class CoinClass : MonoBehaviour
{
    [HideInInspector] public int value;
    [HideInInspector] public bool onPiggy = false;
    [HideInInspector] public Coin coinButton;
    [SerializeField] public TextMeshProUGUI valueText;
    [SerializeField] public RectTransform rect;
    [SerializeField] public Image coinImage;
    
    private CashRegister register;
    private void Awake()
    {
        register = CashRegister.instance;
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag.Equals("Piggy")) 
        {
            register.onPiggy = true;
            register.CoinAmount = value;
            gameObject.SetActive(false);
            --coinButton.CoinsQuantity;
        }
    }
}
