using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using TMPro;

public class Band : MonoBehaviour
{
    [SerializeField] private Transform endOfBand;
    [SerializeField] private TextMeshProUGUI productValue;
    [SerializeField] private Image productImage;
    [SerializeField] private float bandVelocity = default;
    private ProductClass productInstance;
    private CashRegister register;
    public static Band instance;
    private void Awake()
    {
        instance = this;
        Application.targetFrameRate = 120;
    }
    private void Start()
    {
        register = CashRegister.instance;
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag.Equals("Product"))
        {
            register.productOnBand = true;
            productInstance = collision.gameObject.GetComponent<ProductClass>();
            productInstance.onBand = true;
            productInstance.parentButton.SetActive(false);
            collision.transform.position = new Vector2(collision.transform.position.x,
                                                        endOfBand.position.y);
            collision.transform.DOMove(endOfBand.position, bandVelocity).SetEase(Ease.Linear).OnComplete(() =>
            {
                register.productValue = productInstance.value;
                productValue.text = "$" + productInstance.value.ToString();
                productImage.sprite = productInstance.productImage.sprite;
                collision.gameObject.SetActive(false);
            });
        }
    }
    public void ResetBand()
    {
        productValue.text = "$0";
        productImage.sprite = null;
        register.productOnBand = false;
    }
}
