using UnityEngine.EventSystems;
using UnityEngine;
using UnityEngine.UI;
public class Product : MonoBehaviour, IPointerDownHandler, IDragHandler, IPointerUpHandler
{
    [HideInInspector] public ProductClass productInstance;
    private CashRegister register;
    [SerializeField] private int value;
    private Image productImage;
    private Sprite spriteOfProduct;
    private GameObject tempProduct;
    private Utils utils;
    private ProductsControl productsControl;
    void Start()
    {
        utils = Utils.instance;
        productsControl = ProductsControl.instance;
        productImage = GetComponent<Image>();
        spriteOfProduct = productImage.sprite;
        register = CashRegister.instance;
    }
    public void OnPointerDown(PointerEventData eventData)
    {
        if (!register.productOnBand) 
        {
            tempProduct = utils.SpawnFromPool(productsControl.poolDictionary,
                                              "Product",
                                              gameObject.transform.position,
                                              Quaternion.identity);
            productInstance = tempProduct.GetComponent<ProductClass>();
            productInstance.productImage.sprite = spriteOfProduct;
            productInstance.value = value;
            productInstance.parentButton = gameObject;
            tempProduct.transform.position = eventData.position;
        }
        
    }

    public void OnDrag(PointerEventData eventData)
    {
        if (productInstance) 
        {
            if (!productInstance.onBand)
                tempProduct.transform.position = eventData.position;
        }
        

    }
    public void OnPointerUp(PointerEventData eventData)
    {
        if (productInstance) 
        {
            if (!productInstance.onBand)
                tempProduct.SetActive(false);
        }
        
    }
}
