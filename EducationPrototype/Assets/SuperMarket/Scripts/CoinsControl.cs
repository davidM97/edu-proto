using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class CoinsControl : MonoBehaviour
{
    public Dictionary<string, Queue<GameObject>> poolDictionary;
    [SerializeField] private List<Pool> pool;
    private Utils utils;
    [SerializeField] private List<Image> coinButtonList = new List<Image>();
    public static CoinsControl instance;
    private void Awake()
    {
        instance = this;
    }
    private void OnEnable()
    {

    }
    private void Start()
    {
        utils = Utils.instance;
        poolDictionary = utils.PoolObject(pool, gameObject.transform);
        foreach (Image coin in coinButtonList) 
        {
            coin.raycastTarget = false;
            //coin.color = new Color(0.5f,0.5f,0.5f,0.5f);
        }
    }


}
