using UnityEngine;
using UnityEngine.UI;

public class ProductClass : MonoBehaviour
{
    [SerializeField] public Image productImage;
    [HideInInspector] public bool onBand;
    [HideInInspector] public int value;
    [HideInInspector] public GameObject parentButton;
}
