using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProductsControl : MonoBehaviour
{
    [SerializeField] private List<Pool> pool;
    public Dictionary<string, Queue<GameObject>> poolDictionary;
    private Utils utils;
    public static ProductsControl instance;

    private void Awake()
    {
        instance = this;
    }
    private void OnEnable()
    {

    }
    private void Start()
    {
        utils = Utils.instance;
        poolDictionary = utils.PoolObject(pool, gameObject.transform);
    }
}
