using System.Collections;
using System.Collections.Generic;
using Touch = UnityEngine.InputSystem.EnhancedTouch.Touch;
using UnityEngine.InputSystem.EnhancedTouch;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityEngine;
using DG.Tweening;
using TMPro;

public class Coin : MonoBehaviour, IPointerDownHandler, IDragHandler, IPointerUpHandler
{
    [SerializeField] private int value;
    [SerializeField] public int coinsQuantity;
    [SerializeField] private TextMeshProUGUI valueText;
    [SerializeField] private TextMeshProUGUI coinsQuantityText;
    [HideInInspector] public CoinClass coinInstance;
    private CoinsControl coinsControl;
    private CashRegister register;
    private GameObject tempCoin;
    private Utils utils;
    private Vector2 rect;
    private Image coinButtonImage;

    private void OnEnable()
    {
        TouchSimulation.Enable();
    }
    private void OnDisable()
    {
        TouchSimulation.Disable();
    }
    public int CoinsQuantity
    {

        get{return coinsQuantity;}
        set
        {
            coinsQuantity = value;
            coinsQuantityText.text = "x" + coinsQuantity.ToString();
            coinsQuantityText.transform.DOPunchScale(new Vector2(0.8f,0.8f),0.5f,1,1).OnComplete(()=>{
                coinsQuantityText.transform.localScale = new Vector2(1,1);
            });

        }
    }
    private void Start()
    {
        utils = Utils.instance;
        coinsControl = CoinsControl.instance;
        valueText.text = value.ToString();
        coinsQuantityText.text = "x" + coinsQuantity.ToString();
        rect = GetComponent<RectTransform>().rect.size;
        coinButtonImage = GetComponent<Image>();
        register = CashRegister.instance;
    }
    public void OnPointerDown(PointerEventData eventData)
    {
        if (register.productOnBand && coinsQuantity != 0)
        {
            tempCoin = utils.SpawnFromPool(coinsControl.poolDictionary,
                                           "Coin",
                                           gameObject.transform.position,
                                           Quaternion.identity);

            coinInstance = tempCoin.GetComponent<CoinClass>();
            coinInstance.value = value;
            coinInstance.valueText.text = valueText.text;
            coinInstance.rect.sizeDelta = rect;
            coinInstance.coinImage.color = coinButtonImage.color;
            coinInstance.coinButton = this;
            register.onPiggy = false;
            tempCoin.transform.position = eventData.position;
        }


    }
    public void OnDrag(PointerEventData eventData)
    {
        if (coinInstance != null) 
        {
            if (!register.onPiggy)
                tempCoin.transform.position = eventData.position;
        }
        
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        if(coinInstance != null)
            tempCoin.SetActive(false);

    }
}