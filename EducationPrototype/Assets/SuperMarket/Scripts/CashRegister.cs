using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
public class CashRegister : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI coinsText;
    [HideInInspector] public int productValue = default;
    [HideInInspector] public bool productOnBand = default;
    [HideInInspector] public bool onPiggy = default;
    private int[] coinValues = new int[] { 20, 10, 5, 2, 1 };
    [SerializeField] private List<Coin> coinsQuantities = new List<Coin>();
    private int coinAmount = default;
    private Band band;
    public static CashRegister instance;
    private void Awake()
    {
        instance = this;
    }
    private void Start()
    {
        band = Band.instance;
    }
    public int CoinAmount
    {
        get { return coinAmount; }
        set
        {
            coinAmount += value;

            if (coinAmount < productValue)
            {
                coinsText.text = "$" + coinAmount.ToString();
            }
            else if (coinAmount == productValue)
            {

                coinsText.text = "$0";
                coinAmount = 0;
                band.ResetBand();
            }
            else if (coinAmount > productValue)
            {
                coinsText.text = "$0";
                CalculateChange(coinAmount - productValue);
            }
        }
    }
    public bool ProductOnBand
    {
        get { return productOnBand; }
        set
        {
            productOnBand = value;
        }
    }
    public void CalculateChange(int total)
    {
        int i = 0;
        while (total != 0)
        {
            i++;
            if (i >= coinValues.Length)
                i = 0;
            if (total % coinValues[i] == 0)
            {
                total -= coinValues[i];
                ++coinsQuantities[i].CoinsQuantity;
                print(coinValues[i]);
            }
        }
        coinAmount = 0;
        band.ResetBand();
    }
}
