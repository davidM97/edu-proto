using UnityEngine.Networking;
using Firebase.Storage;
using UnityEngine.UI;
using UnityEngine;
using System.Threading.Tasks;
using System.Runtime.CompilerServices;
using UnityEngine.AddressableAssets;
using RobinBird.FirebaseTools.Storage.Addressables;
public class FireBaseStorageManager : MonoBehaviour
{
    [SerializeField] RawImage logo;
    private FirebaseStorage storage;
    private StorageReference dbReference;
    private StorageReference imageLogoDB;
    public static FireBaseStorageManager instance;
    private void Awake()
    {
        instance = this;
        
        
    }

    public async Task LoadImage(string mediaUrl)
    {
        Addressables.ResourceManager.ResourceProviders.Add(new FirebaseStorageAssetBundleProvider());
        Addressables.ResourceManager.ResourceProviders.Add(new FirebaseStorageJsonAssetProvider());
        Addressables.ResourceManager.ResourceProviders.Add(new FirebaseStorageHashProvider());

        // This requires Addressables >=1.75 and can be commented out for lower versions
        Addressables.InternalIdTransformFunc += FirebaseAddressablesCache.IdTransformFunc;
        FirebaseAddressablesManager.IsFirebaseSetupFinished = true;


        storage = FirebaseStorage.DefaultInstance;
        dbReference = storage.GetReferenceFromUrl("https://firebsestorage.googleapis.com/v0/b/educationalgame-98c46.appspot.com/o/");
        print(dbReference.ToString());
        imageLogoDB = dbReference.Child(mediaUrl);
        print(imageLogoDB.ToString());
        await imageLogoDB.GetDownloadUrlAsync();

        UnityWebRequest request = UnityWebRequestTexture.GetTexture(imageLogoDB.ToString());
        print(request.ToString());
        await request.SendWebRequest();

        //if (request.result == UnityWebRequest.Result.ConnectionError || 
        //    request.result == UnityWebRequest.Result.ProtocolError)
        //{
        //    Debug.LogError(request.error);
        //}
        //else
            logo.texture = ((DownloadHandlerTexture)request.downloadHandler).texture;
        
        //Task test = imageLogoDB.GetDownloadUrlAsync()(task =>
        //{
        //    if (!task.IsFaulted && !task.IsCanceled)
        //    {
        //        mediaUrl = Convert.ToString(task.Result);
        //        print(mediaUrl);
        //    }
        //    else
        //    {
        //        Debug.LogError(task.Exception);
        //    }
        //});


        //UnityWebRequest request = UnityWebRequestTexture.GetTexture(mediaUrl);
        //yield return request.SendWebRequest();
        //if (request.isNetworkError || request.isHttpError)
        //{
        //    Debug.LogError(request.error);
        //}
        //else 
        //{
        //    logo.texture = ((DownloadHandlerTexture)request.downloadHandler).texture;
        //}

    }
}
public static class ExtensionMethods
{
    public static TaskAwaiter GetAwaiter(this AsyncOperation asyncOp)
    {
        var tcs = new TaskCompletionSource<object>();
        asyncOp.completed += obj => { tcs.SetResult(null); };
        return ((Task)tcs.Task).GetAwaiter();
    }
}
