using UnityEngine;
using System.Collections;
using Firebase.Auth;
using Firebase.Database;
using Firebase;
using TMPro;
using System.Linq;

public class FireBaseManager : MonoBehaviour
{
    [Header("Firebase")]
    public DependencyStatus dependencyStatus;
    private FirebaseAuth auth;
    private FirebaseUser user;
    private DatabaseReference dbReference;

    [Header("Login")]
    [SerializeField] private TMP_InputField emailLoginField;
    [SerializeField] private TMP_InputField passwordLoginField;
    [SerializeField] private TMP_Text warningLoginText;
    [SerializeField] private TMP_Text confirmLoginText;

    [Header("Register")]
    [SerializeField] private TMP_InputField usernameRegisterFailed;
    [SerializeField] private TMP_InputField emailRegisterFailed;
    [SerializeField] private TMP_InputField passwordRegisterField;
    [SerializeField] private TMP_InputField passwordRegisterVerifyField;
    [SerializeField] private TMP_Text warningRegisterText;

    [Header("UserData")]
    [SerializeField] private TMP_InputField userNameField;
    [SerializeField] private TMP_InputField xpField;
    [SerializeField] private TMP_InputField killsField;
    [SerializeField] private TMP_InputField deathsField;
    [SerializeField] private TMP_Dropdown schoolList;
    [SerializeField] private GameObject scoreElement;
    [SerializeField] private Transform scoreBoardContent;

    private void Awake()
    {
        FirebaseApp.CheckAndFixDependenciesAsync().ContinueWith(task =>
        {
            dependencyStatus = task.Result;
            if (dependencyStatus == DependencyStatus.Available)
            {
                InitializeFireBase();

            }
            else
            {
                Debug.LogError("LOL no me conecte " + dependencyStatus);
            }
        });
    }
    private void InitializeFireBase()
    {
        auth = FirebaseAuth.DefaultInstance;
        dbReference = FirebaseDatabase.DefaultInstance.RootReference;
        
    }
    public void ClearLoginFields()
    {
        emailLoginField.text = "";
        passwordLoginField.text = "";
    }
    public void ClearRegisterFields()
    {
        usernameRegisterFailed.text = "";
        emailRegisterFailed.text = "";
        passwordRegisterField.text = "";
        passwordRegisterVerifyField.text = "";
    }
    public void LoginButton()
    {
        StartCoroutine(Login(emailLoginField.text, passwordLoginField.text));
    }
    public void RegisterButton()
    {
        StartCoroutine(Register(emailRegisterFailed.text, passwordRegisterField.text, usernameRegisterFailed.text));
    }
    public void SignOutButton()
    {
        auth.SignOut();
        ClearRegisterFields();
        ClearLoginFields();
    }
    public void ScoreBoardButton()
    {
        StartCoroutine(LoadScoreBoardData());
    }
    public void SaveDataButton()
    {
        StartCoroutine(UpdateUserData(userNameField.text, schoolList.options[schoolList.value].text));
        //StartCoroutine(UpdateUsernameDatabase(userNameField.text));

        //StartCoroutine(UpdateXp(int.Parse(xpField.text)));
        //StartCoroutine(UpdateKills(int.Parse(killsField.text)));
        //StartCoroutine(UpdateDeaths(int.Parse(deathsField.text)));

    }
    private IEnumerator Login(string _email, string _password)
    {
        var LoginTask = auth.SignInWithEmailAndPasswordAsync(_email, _password);

        yield return new WaitUntil(predicate: () => LoginTask.IsCompleted);
        if (LoginTask.Exception != null)
        {
            Debug.LogWarning(message: $"Failed to register task with {LoginTask.Exception}");
            FirebaseException firebaseEx = LoginTask.Exception.GetBaseException() as FirebaseException;

            AuthError errorCode = (AuthError)firebaseEx.ErrorCode;

            string message = "Login Failed!";

            switch (errorCode)
            {

                case AuthError.MissingEmail:
                    message = "Missing Email";
                    break;
                case AuthError.MissingPassword:
                    message = "Missing Password";
                    break;
                case AuthError.WrongPassword:
                    message = "Wrong Password";
                    break;
                case AuthError.InvalidEmail:
                    message = "Invalide Email";
                    break;
                case AuthError.UserNotFound:
                    message = "Account does not exist";
                    break;
            }
            warningLoginText.text = message;
        }
        else
        {
            user = LoginTask.Result;
            FireBaseStorageManager.instance.LoadImage("logo.png");
            Debug.LogFormat("User signed in successfully: {0}({1})", user.DisplayName, user.Email);
            warningLoginText.text = "";
            confirmLoginText.text = "Logged In";

            userNameField.text = user.DisplayName;
            ClearLoginFields();
            ClearRegisterFields();
        }
    }
    private IEnumerator Register(string _email, string _password, string _username)
    {
        if (_username == "")
            warningRegisterText.text = "Missing UserName";
        else if (passwordRegisterField.text != passwordRegisterVerifyField.text)
            warningRegisterText.text = "Password Does Not Match!";
        else
        {
            var RegisterTask = auth.CreateUserWithEmailAndPasswordAsync(_email, _password);

            yield return new WaitUntil(predicate: () => RegisterTask.IsCompleted);

            if (RegisterTask.Exception != null)
            {
                Debug.LogWarning(message: $"Failed to register task with {RegisterTask.Exception}");
                FirebaseException firebaseEx = RegisterTask.Exception.GetBaseException() as FirebaseException;
                AuthError errorCode = (AuthError)firebaseEx.ErrorCode;
                string message = "Register Failed!";
                switch (errorCode)
                {

                    case AuthError.WeakPassword:
                        message = "Missing Email";
                        break;
                    case AuthError.MissingEmail:
                        message = "Missing Password";
                        break;
                    case AuthError.MissingPassword:
                        message = "Weak Password";
                        break;
                    case AuthError.EmailAlreadyInUse:
                        message = "Email Already In Use";
                        break;
                }
                warningRegisterText.text = message;
            }
            else
            {
                user = RegisterTask.Result;

                if (user != null)
                {
                    UserProfile profile = new UserProfile { DisplayName = _username };
                    var ProfileTask = user.UpdateUserProfileAsync(profile);
                    yield return new WaitUntil(predicate: () => ProfileTask.IsCompleted);

                    if (ProfileTask.Exception != null)
                    {
                        Debug.LogWarning(message: $"Failed to register task with {ProfileTask.Exception}");
                        FirebaseException firebaseEx = ProfileTask.Exception.GetBaseException() as FirebaseException;
                        AuthError errorCode = (AuthError)firebaseEx.ErrorCode;
                        warningRegisterText.text = "Username Set Failed!";
                    }
                    else
                    {
                        warningRegisterText.text = "";
                        ClearLoginFields();
                        ClearRegisterFields();
                    }
                }
            }
        }
    }
    private IEnumerator UpdateUserData(string _userName, string _schoolName)
    {
        UserProfile profile = new UserProfile { DisplayName = _userName };

        var ProfileTask = user.UpdateUserProfileAsync(profile);

        yield return new WaitUntil(predicate: () => ProfileTask.IsCompleted);

        if (ProfileTask.Exception != null)
        {
            Debug.LogWarning(message: $"Failed to register task with {ProfileTask.Exception}");
        }
        var DBTask = dbReference.Child("users").Child(user.UserId).Child("teacherName").SetValueAsync(_userName);

        yield return new WaitUntil(predicate: () => DBTask.IsCompleted);

        var DBShool = dbReference.Child("users").Child(user.UserId).Child("schoolName").SetValueAsync(_schoolName);

        yield return new WaitUntil(predicate: () => DBShool.IsCompleted);

        var UniqueKey = dbReference.Child("users").Child(user.UserId).Push().Key;
        var DBUniqueKey = dbReference.Child("users").Child(user.UserId).Child("Key").SetValueAsync(UniqueKey);
        dbReference.Child("users").Child("LOOOL").SetValueAsync(UniqueKey);
        yield return new WaitUntil(predicate: () => DBUniqueKey.IsCompleted);

        if (DBTask.Exception != null || DBShool.Exception != null || DBUniqueKey.Exception != null)
            Debug.LogWarning(message: $"Failed to register task with {DBTask.Exception} and {DBShool.Exception} and {DBUniqueKey.Exception}");

        else
            Debug.Log("Success");
        
    }

    private IEnumerator LoadScoreBoardData()
    {
        var DBTask = dbReference.Child("users").OrderByChild("kills").GetValueAsync();

        yield return new WaitUntil(predicate: () => DBTask.IsCompleted);

        if (DBTask.Exception != null)
            Debug.LogWarning(message: $"Failed to register task with {DBTask.Exception}");
        else
        {
            DataSnapshot snapshot = DBTask.Result;
            foreach (Transform child in scoreBoardContent.transform)
            {
                Destroy(child.transform);
            }
            foreach (DataSnapshot childSnapShot in snapshot.Children.Reverse<DataSnapshot>())
            {
                string userName = childSnapShot.Child("username").Value.ToString();
                int kills = int.Parse(childSnapShot.Child("kills").Value.ToString());
                int deaths = int.Parse(childSnapShot.Child("deaths").Value.ToString());
                int xp = int.Parse(childSnapShot.Child("xp").Value.ToString());
                Debug.Log(message: $"{userName},{kills},{deaths},{xp}");
                GameObject scoreboardElement = Instantiate(scoreElement, scoreBoardContent);
                scoreboardElement.GetComponent<ScoreElement>().NewScoreElement(userName, kills, deaths, xp);
            }
        }
    }
}
